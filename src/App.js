import React, { useEffect } from 'react';
import './App.css';
import { connect } from 'react-redux';
import * as actions from './store/actions/auth';
import Layout from './component/UI/Layout/Layout';
import { Route, Switch, withRouter, Redirect } from 'react-router-dom';
import Auth from './containers/auth/Auth';
import Blog from './component/blog/Blog'
import Dashboard from './component/dashboard/Dashboard'
import Navbar from './component/dashboard/Navbar/Navbar';
import Logout from './containers/auth/Logout/Logout'

const App = (props) => {
 
  useEffect(() => {
      props.checkValididty();
  },[]);

  let routes = (
    <Switch>
      <Route path="/login" component={Auth}/> 
      <Route path="/" exact component={Blog}/>
      <Redirect to="/" />
    </Switch>
  );
  // if user is logedin disable login route with auth component
  if( props.authenticated ) {
    routes = (
      <Switch>
        <Route path="/admin" component={Dashboard}/>
        <Route path="/login">
          <Redirect to="/admin" />
        </Route> 
        <Route path="/logout" component={Logout} />
        <Route path="/" exact component={Blog}/>
        <Redirect to="/" />
      </Switch>
    );
  }

    return (
      <div className="App">
        <Layout>
          <Navbar />
          {routes}
        </Layout>
      </div>
    );
  }


const mapDispatchToProps = dispatch => {
  return {
    checkValididty: () => dispatch(actions.checkIfLoggedIn()),
  }
}

const mapStateToProps = state => {
  return {
    authenticated: state.auth.token !== null,
  }
}

export default withRouter(connect(mapStateToProps, mapDispatchToProps)(App));
