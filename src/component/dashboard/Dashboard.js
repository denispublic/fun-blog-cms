import React, { useState, useEffect } from 'react';
import { Button, Input, Container, makeStyles } from '@material-ui/core';
import * as utilities from '../../shared/utility'
import { connect } from 'react-redux';
import * as actions from '../../store/actions/blogPost';
import Auxiliary from '../../hoc/Auxiliary';
import CircularProgress from '@material-ui/core/CircularProgress';
import Post from './Post/Post'

const useStyles = makeStyles({
    container: {
        'background': '#e1f5fe',
        'max-width': '400px',
        'box-shadow':' 0px 1px 2px #8aa5b0',
        'padding':'30px',
        'margin':'50px'
    },
    form: {
        'display': 'flex',
        'flex-direction': 'column',
        'width': '300px',
        'margin': 'auto'
    },
    button: {
        'margin': '10px 0',
        'font-size' : '14px'
    }
  });

const Dashboard = (props) => {

    const classes = useStyles();

    const [state, setState] = useState({
        controls: {
            postTitle: {
                elementType: 'Input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Title'
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            postContent: {
                elementType: 'Input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Content'
                },
                value: '',
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
        },
        validForm: false
    }) 

    useEffect( () => {
        props.getAllPosts()
    }, [])

    let postsArray = <CircularProgress />;
    if(props.postsList !== null) {
        postsArray = (props.postsList.map(post => (
            <Container key={post.id} className={classes.container}>
                <Post   title={post.title}
                        content={post.content}
                        id={post.id}
                        delete={() => props.onDeletePost(post.id) }
                        update={(title, content, postId) => updatePost(title, content, postId) }/>
            </Container>
        ))
        )
    }

    const inputChangeHanlder = (event, elementToUpdate) => {
        const updatedControls = {
          ...state.controls,
          [elementToUpdate]: {
            ...state.controls[elementToUpdate],
            value: event.target.value,
            valid: utilities.validationCheck(event.target.value, state.controls[elementToUpdate].validation),
            touched: true,
          }
        };
        setState({
          ...state,
          controls: updatedControls
        })
      };

    
    const submitForm = (event) => {
        event.preventDefault();
        let title = state.controls.postTitle.value;
        let content = state.controls.postContent.value;
        props.onAddNewPost(title, content);
    }

    const updatePost = (title, content, postId) => {
        const postData = {
            title: title,
            content: content
        }
        props.onUpdatePost(postId, postData);
    }

    const formElementsArr = [];
    for(let key in state.controls) {
        formElementsArr.push({
            id: key,
            config: state.controls[key]
        })
    }


    let form = formElementsArr.map( formElem => (
        <Input  required
                placeholder={formElem.config.elementConfig.placeholder}
                key={formElem.id}
                type={formElem.config.elementConfig.type}    
                onChange={(event) => inputChangeHanlder(event, formElem.id) }/>
        )) 

        

    return ( 
        <Auxiliary>
            <Container className={classes.container}>
                <form className={classes.form}>
                    {form} 
                    <Button onClick={ submitForm } 
                            color="primary" 
                            variant="outlined"
                            size="medium"
                            className={classes.button}>
                            Submit
                    </Button>
                </form>
            </Container>
            {postsArray}
        </Auxiliary>
    );
};

const mapStateToProps = state => {
    return {
        authenticated: state.auth.token !== null,
        postsList: state.blog.posts
    }
}

const mapDispacthToProps = dispatch => {
    return {
        getAllPosts: () => dispatch( actions.getAllPosts() ),
        onAddNewPost: (title, content) => dispatch( actions.addNewPost(title, content) ),
        onDeletePost: ( postId ) => dispatch(actions.deletePost( postId )),
        onUpdatePost: ( postId, postData ) => dispatch(actions.updatePost( postId, postData ))
    }
}

export default connect(mapStateToProps, mapDispacthToProps)(Dashboard);