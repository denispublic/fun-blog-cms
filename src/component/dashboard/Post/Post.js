import React, {useState} from 'react';
import Auxiliary from '../../../hoc/Auxiliary';
import { Button, Input, Container, makeStyles } from '@material-ui/core';
import * as utilities from '../../../shared/utility'

const useStyles = makeStyles({
    container: {
        'background': '#e1f5fe',
        'max-width': '400px',
        'box-shadow':' 0px 1px 2px #8aa5b0',
        'padding':'30px',
        'margin':'50px'
    },
    form: {
        'display': 'flex',
        'flex-direction': 'column',
        'width': '300px',
        'margin': 'auto'
    },
    button: {
        'margin': '10px 0',
        'font-size' : '14px'
    }
  });

const Post = (props) => {
    const classes = useStyles();
    
    const [state, setState] = useState({
        controls: {
            postTitle: {
                elementType: 'Input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Title'
                },
                value: props.title,
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
            postContent: {
                elementType: 'Input',
                elementConfig: {
                    type: 'text',
                    placeholder: 'Content'
                },
                value: props.content,
                validation: {
                    required: true,
                },
                valid: false,
                touched: false
            },
        },
        validForm: false,
        editMode: false
    })

    const inputChangeHanlder = (event, elementToUpdate) => {
        const updatedControls = {
          ...state.controls,
          [elementToUpdate]: {
            ...state.controls[elementToUpdate],
            value: event.target.value,
            valid: utilities.validationCheck(event.target.value, state.controls[elementToUpdate].validation),
            touched: true,
          }
        };
        setState({
          ...state,
          controls: updatedControls
        })
      };

    const formElementsArr = [];
    for(let key in state.controls) {
        formElementsArr.push({
            id: key,
            config: state.controls[key]
        })
    }
  
    let form = formElementsArr.map( formElem => (
        <Input  required
                placeholder={formElem.config.elementConfig.placeholder}
                key={formElem.id}
                type={formElem.config.elementConfig.type}  
                value={formElem.config.value}  
                onChange={(event) => inputChangeHanlder(event, formElem.id) }/>
        )) 

    const switchToEdit = () => {
        setState({
            ...state,
            editMode: !state.editMode
        })
    }

    const updatePost = (title, content) => {
        props.update(title, content, props.id );
        switchToEdit();
    }
    let singlePost = state.editMode ? (
        <Container>
            <form className={classes.form}>
                {form} 
                <Button onClick={() => updatePost(state.controls.postTitle.value, state.controls.postContent.value, props.id )}
                        color="primary" 
                        variant="outlined"
                        size="medium"
                        className={classes.button}>
                        Submit
                </Button>
            </form>
        </Container>
    ) : (
        <Auxiliary>
            <h1>{props.title}</h1>
            <p>{props.content}</p>
            <Button variant="outlined" color="secondary" onClick={() => props.delete(props.id) }>Delete post</Button>
        </Auxiliary>
    )

    return (
       <Auxiliary>
           {singlePost}
            <Button variant="outlined" color="primary" onClick={() => switchToEdit() } > Edit </Button>
       </Auxiliary>
    );
};

export default Post;