import React from 'react';
import { NavLink } from 'react-router-dom';
import { connect } from 'react-redux'

const Navbar = (props) => {

    return (
        <div>
            <NavLink to="/">Home</NavLink>
            { props.authenticated ? <NavLink to="/admin">Dashboard</NavLink> : null }       
            { props.authenticated ? 
                <NavLink to="/logout">Logout</NavLink> : 
                <NavLink to="/login">Login</NavLink> }
        </div>
    );
};

  
  const mapStateToProps = state => {
    return {
      authenticated: state.auth.token !== null,
    }
  }
  
  export default connect(mapStateToProps, null)(Navbar);