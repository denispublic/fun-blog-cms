import React from 'react';
import Auxiliary from '../../../hoc/Auxiliary';

const Layout = (props) => {


    return (
        <Auxiliary>
            {props.children}
        </Auxiliary>
        )
      
};

export default Layout;