import React, { useEffect, useState } from 'react';
import * as actions from '../../store/actions/blogPost';
import { connect } from 'react-redux';
import CircularProgress from '@material-ui/core/CircularProgress';

const Blog = (props) => {

    useEffect( () => {
        props.getAllPosts()
    }, [])

    let postsArray = <CircularProgress />;
    if(props.postsList !== null) {
        postsArray = (props.postsList.map(post => (
            <div key={post.id}>
                <h1>{post.title}</h1>
                <p>{post.content}</p>
            </div>
        ))
        )
    }
    
    return (
        <div>
            <h1>This is a blog</h1>
            { postsArray }
        </div>
    );
};

const mapStateToProps = state => {
    return {
        authenticated: state.auth.token !== null,
        postsList: state.blog.posts
    }
}

const mapDispatchToProps = dispatch => {
    return {
        getAllPosts: () => dispatch( actions.getAllPosts() )
    }
}

export default connect( mapStateToProps, mapDispatchToProps )(Blog);