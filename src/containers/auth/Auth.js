import React, { Component } from 'react';
import { Button, Input, Container } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/auth'
import * as utilities from '../../shared/utility'
import logo from '../../assets/img/feather.png';

const styles = {
    container: {
        'background': '#e1f5fe',
        'max-width': '400px',
        'box-shadow':' 0px 1px 2px #8aa5b0',
        'padding':'30px'
    },
    form: {
        'display': 'flex',
        'flex-direction': 'column',
        'width': '300px',
        'margin': 'auto'
    },
    button: {
        'margin': '10px 0',
        'font-size' : '14px'
    }
  };


class Auth extends Component {
    state = {
        controls: {
            email: {
                elementType: 'Input',
                elementConfig: {
                    type: 'email',
                    placeholder: 'Mail Address'
                },
                value: '',
                validation: {
                    required: true,
                    isEmail: true
                },
                valid: false,
                touched: false
            },
            password: {
                elementType: 'Input',
                elementConfig: {
                    type: 'password',
                    placeholder: 'Password'
                },
                value: '',
                validation: {
                    required: true,
                    minLength: 6
                },
                valid: false,
                touched: false
            },
        },
        isSignup: false,
        validForm: false
    }  


    inputChangeHanlder(e, elementToUpdate) {
        const updatedForm = {
            ...this.state.controls
        };
        const updatedFormElement = {
            ...updatedForm[elementToUpdate]
        };
        
        updatedFormElement.value = e.target.value;

        updatedFormElement.valid = utilities.validationCheck(updatedFormElement.value, updatedFormElement.validation)
        
        updatedFormElement.touched = true;

        updatedForm[elementToUpdate] = updatedFormElement;

        this.setState({controls: updatedForm});

        /*
        * After updating the state, check to see if the form is valid
        */
       let formIsValid;
       for (let key in this.state.controls) {
           this.state.controls[key].valid ? formIsValid = true : formIsValid = false;
       }
       this.setState({validForm: formIsValid})
    }

    switchSignup = () => {
        let isSignup = this.state.isSignup;
        isSignup = !isSignup;
        this.setState({...this.state, isSignup: isSignup})
    }

    submitForm = (event) => {
        event.preventDefault();
        if(this.state.validForm) {
            this.props.onAuth(this.state.controls.email.value, this.state.controls.password.value, this.state.isSignup)
        }
    }

    render() { 
        const { classes } = this.props;

        const formElementsArr = [];
        for(let key in this.state.controls) {
            formElementsArr.push({
                id: key,
                config: this.state.controls[key]
            })
        }

        let form = formElementsArr.map( formElem => (
        <Input  required
                placeholder={formElem.config.elementConfig.placeholder}
                key={formElem.id}
                type={formElem.config.elementConfig.type}    
                onChange={(event) => this.inputChangeHanlder(event, formElem.id) }/>
        )) 
        return (
            <Container className={classes.container}>
                <div>
                    <Button onClick={ this.switchSignup } 
                            color="primary" 
                            variant="contained"
                            size="small"
                            className={classes.button}>
                            { this.state.isSignup ? 'Switch to Sign in' : 'Swtich to Sign up'}
                    </Button>
                </div>
                <img src={logo} alt="logo" width="100px"/>
                <form className={classes.form}>
                    {form} 
                    <Button onClick={ this.submitForm } 
                            disabled={!this.state.validForm} 
                            color="primary" 
                            variant="outlined"
                            size="medium"
                            className={classes.button}>
                            {this.state.isSignup ? 'Sign Up' : 'Sign In'}
                    </Button>
                </form>
            </Container>
        )
    }
}

Auth.propTypes = {
    classes: PropTypes.object.isRequired,
  };

const mapStateToProps = state => {
    return {
        authenticated: state.auth.token !== null
    }
}
const mapDispacthToProps = dispatch => {
    return {
        onAuth: (email, password, isSignup) => dispatch( actions.auth(email, password, isSignup) )
    }
}

export default connect( mapStateToProps, mapDispacthToProps )( withStyles(styles)(Auth) );