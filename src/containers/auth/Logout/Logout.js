import React, {useEffect} from "react";
import * as actios from '../../../store/actions/auth';
import {connect} from 'react-redux';
import { Redirect } from 'react-router-dom'

const Logout = (props) => {
  useEffect(() => {
    props.onLogout();
  }, []);

  return (
    <Redirect to="/login" />
  );
};

const mapDispatchToProps = dispatch => {
  return {
    onLogout: () => dispatch(actios.signout())
  }
};

export default connect(null, mapDispatchToProps)(Logout);