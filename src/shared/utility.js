// These are shared functions that can be use multiple times in project
export const updateObject = (oldObject, updatedProperties) => {
    return {
        ...oldObject,
        ...updatedProperties
    };
};

export const  validationCheck = ( value, rules ) => {
    let isValid = true;

     // end validation if there are no rules
    if( !rules ) {
        return true;
    }

    // check if required field is empty
    if( rules.required ) {
        isValid = value.trim() !== '';
    }

    /* 
    * check if email matches the pattern
    * feel free to change the pattern
    */
    if( rules.isEmail ) {
    const pattern = /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
    isValid = pattern.test(value);
    }

    /*
    * check a password between 6 to 20 characters 
    * which contain at least one numeric digit, 
    * one uppercase and one lowercase letter
    * feel free to change the pattern
    */
    if( rules.isPassword ) {
    const pattern = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{6,20}$/;
    isValid = pattern.test(value);
    }

    return isValid;

}