import axios from 'axios';
import * as actionTypes from './actionTypes';

export const addNewPostStart = () => {
    return {
        type: actionTypes.ADD_NEW_POST_START
    }
}

export const addNewPostSuccess = () => {
    return {
        type: actionTypes.ADD_NEW_POST_SUCCESS
    }
}

export const addNewPostFailed = () => {
    return {
        type: actionTypes.ADD_NEW_POST_FAILED
    }
}

export const getAllPostsStart = () => {
    return {
        type: actionTypes.GET_ALL_POSTS_START
    }
}

export const getAllPostsSuccess = (postsArr) => {
    return {
        type: actionTypes.GET_ALL_POSTS_SUCCESS,
        posts: postsArr
    }
}

export const getAllPostsFailed = () => {
    return {
        type: actionTypes.GET_ALL_POSTS_FAILED
    }
}

export const deletePostStart = () => {
    return {
        type: actionTypes.DELETE_POST_START
    }
}

export const deletePostSuccess = () => {
    return {
        type: actionTypes.DELETE_POST_SUCCESS
    }
}

export const deletePostFailed = () => {
    return {
        type: actionTypes.DELETE_POST_FAIL
    }
}

export const updatePostStart = () => {
    return {
        type: actionTypes.UPDATE_POST_START
    }
}

export const updatePostSuccess = () => {
    return {
        type: actionTypes.UPDATE_POST_SUCCESS
    }
}

export const updatePostFailed = () => {
    return {
        type: actionTypes.UPDATE_POST_FAIL
    }
}

export const addNewPost = (title, content) => {
    return dispatch => {
        dispatch(addNewPostStart);

        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');
        // prepare post data 
        const postData = {
            title,
            content,
            userId
        }
        const url = `https://fun-blog-cms.firebaseio.com/posts.json?auth=${token}`;

        axios.post(url, postData)
        .then(res => {
            dispatch(addNewPostSuccess);
            if(localStorage.getItem('token')) {
                dispatch(getAllAuthPost());
            } else {
                dispatch(getAllPosts());
            }
        })
        .catch(err => {
            console.log(err)
        })
    }
}

export const getAllAuthPost = () => {
    return dispatch => {
        dispatch(getAllPostsStart);
        const token = localStorage.getItem('token');
        const userId = localStorage.getItem('userId');
        const queryParams = `?auth=${token}&orderBy="userId"&equalTo="${userId.toString()}"`;
        const url = `https://fun-blog-cms.firebaseio.com/posts.json${queryParams}`;
        axios.get(url)
        .then(res => {
            let postsArr = [];
            for (let key in res.data) {
                postsArr.push({
                    title: res.data[key].title,
                    content: res.data[key].content,
                    id: key
                })
            }
            dispatch(getAllPostsSuccess(postsArr))
        })
        .catch(err => {
            console.log(err)
        })
    }
}

export const getAllPosts = () => {
    return dispatch => {
        dispatch(getAllPostsStart);
        const url = `https://fun-blog-cms.firebaseio.com/posts.json`;

        axios.get(url)
            .then(res => {
                let postsArr = [];
                for (let key in res.data) {
                    postsArr.push({
                        title: res.data[key].title,
                        content: res.data[key].content,
                        id: key
                    })
                }
                dispatch(getAllPostsSuccess(postsArr));
            })
            .catch(err => {
                console.log(err)
            })
    }
}

export const deletePost = (postId) => {
    console.log(postId)
    return dispatch => {
        dispatch(getAllAuthPost)
        const token = localStorage.getItem('token');
        const url = `https://fun-blog-cms.firebaseio.com/posts/${postId}.json?auth=${token}`;
        axios.delete(url)
        .then(res => {
            console.log(res)
            dispatch(getAllPosts())
        })
        .catch(err => {
            console.log(err)
        })
    }
}


export const updatePost = (postId, postData) => {
    return dispatch => {
        dispatch(updatePostStart);
        const token = localStorage.getItem('token');
        const url = `https://fun-blog-cms.firebaseio.com/posts/${postId}.json?auth=${token}`;
        console.log(postData)
        axios.put(url, postData)
        .then(res => {
            dispatch(getAllPosts())
            console.log(res);
        })
        .catch(err => {
            console.log(err)
        })
    }
}