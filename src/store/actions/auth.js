import * as actionTypes from './actionTypes';
import axios from 'axios';

export const authStart = () => {
    return {
        type: actionTypes.AUTH_START
    }
}

export const authSuccess = (token, localId) => {
    return {
        type: actionTypes.AUTH_SUCCESS,
        token: token,
        userId: localId
    }
}

export const authFailed = (emailMess, passMess) => {
    return {
        type: actionTypes.AUTH_FAILED,
        emailMess: emailMess,
        passMess: passMess
    }
}

export const signout = () => {
    localStorage.removeItem('exparationDate');
    localStorage.removeItem('token');
    localStorage.removeItem('userId');
    return {
        type: actionTypes.SIGNOUT,
    }
}


// trigers for login and signup
export const auth = (email, password, isSignup) => {
    return dispatch => {
        dispatch(authStart());
        const formData = {
            email: email,
            password: password,
            returnSecureToken: true
        };
        let url = isSignup ? 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyB9dZKR6-8df-D1xItzl5NIieLCH7PHzks' : 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyB9dZKR6-8df-D1xItzl5NIieLCH7PHzks'
        axios.post(url, formData)
            .then(res => {
                // put data in local storage
                const expirationDate = new Date(new Date().getTime() + res.data.expiresIn * 1000);
                localStorage.setItem('exparationDate', expirationDate)
                localStorage.setItem('token', res.data.idToken);
                localStorage.setItem('userId', res.data.localId);
                // dispatch action to populate state with user data
                dispatch(authSuccess(res.data.idToken, res.data.localId));
            })
            .catch(error => {
                if( error.response.data.error.message === 'INVALID_EMAIL') {
                    dispatch(authFailed('Invalid email address'), null)
                } else if ( error.response.data.error.message === 'INVALID_PASSWORD') {
                    dispatch(authFailed(null, 'Invalid password'))
                }
            })
    }
}


// check if user session is expired
export const checkIfLoggedIn = () => {
    return dispatch => {
        const currentDate = new Date();
        const expirationDate = new Date(localStorage.getItem('exparationDate'));
        if (currentDate < expirationDate ) {
            dispatch(authSuccess(localStorage.getItem('token'), localStorage.getItem('userId')));
        } else {
            dispatch(signout());
        }
    }
}