
import * as actionTypes from '../actions/actionTypes';

const initialState = {
    token: null,
    userId: null,
    errors: {
        emailMess: '',
        passMess: ''
    },
};

function authReducer(state = initialState, action) {
        switch (action.type) {
            case actionTypes.AUTH_SUCCESS:
                // if authentication successfull, get user token and ID from axios response
                return {
                    ...state,
                   token: action.token,
                   userId: action.userId
                }
            case actionTypes.AUTH_FAILED:
                // if authentication failed, get error message
                return {
                    ...state,
                    errors: {
                        emailMess: action.emailMess,
                        passMess: action.passMess
                    }
                }
            case actionTypes.SIGNOUT: 
                // if user signout, clear token and userID
                return {
                    ...state,
                    token: null,
                    userId: null
                }
            default:  return state;
        }
}

export default authReducer;