import * as actionTypes from '../actions/actionTypes';

const initialState = {
    posts: null
}

function blogReducer(state = initialState, action) {
    switch(action.type) {
        case actionTypes.GET_ALL_POSTS_SUCCESS:
            return {
                ...state,
                posts: action.posts
            }
        default: 
            return state;
    }
}

export default blogReducer;